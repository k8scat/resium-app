# resium-app

基于 uni-app 实现的源自下载小程序

## Todo

* [ ] 搜索框优化
	重置value
* [ ] 源说
* [ ] Sentry
	
## Icon

from: https://www.iconfont.cn/
size: 200x200
default color: #cdcdcd
selected color: #5cadff

## uni.request 封装

https://quanzhan.co/luch-request/guide/3.x/

## uni-app 插件

* [List 列表](https://ext.dcloud.net.cn/plugin?id=24)
* [Card 卡片](https://ext.dcloud.net.cn/plugin?id=22)
* [SearchBar 搜索栏](https://ext.dcloud.net.cn/plugin?id=866)
* [Loading](https://ext.dcloud.net.cn/plugin?id=1190)

## Tools

* [iconfont](https://www.iconfont.cn/)

## https://github.com/Tencent/weui-wxss

## References

* [页面生命周期](https://uniapp.dcloud.io/frame?id=%E9%A1%B5%E9%9D%A2%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F)
* [uni-scss](https://uniapp.dcloud.io/collocation/uni-scss)

## uni-app

* [开发者后台](https://dev.dcloud.net.cn/app/index?type=0)
* [UniCloud 云函数](https://unicloud.dcloud.net.cn/home)
* [UniAD 广告](https://uniad.dcloud.net.cn/home)
